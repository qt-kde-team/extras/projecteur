# Fallback for version generation from pure git archive exports

set(GIT_EXPORT_VERSION_SHORTHASH "a7b2649")
set(GIT_EXPORT_VERSION_FULLHASH "a7b2649296f5f8b6793e0c38cdee93d36831404b")
set(GIT_EXPORT_VERSION_BRANCH "tag: v0.10, master") # needs parsing in cmake...
set(GIT_EXPORT_VERSION_DATE_MONTH_YEAR "2023-10-17")
set(HAS_GIT_EXPORT_INFO 1)
